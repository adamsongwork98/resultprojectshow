export function filterJackpotNumber(data) {
  let newString = data || ''
  if (newString.substring(0, 2).toUpperCase() === 'RM') {
    newString = newString.substring(2, newString.length)
  }
  if (newString.length >= 4 && !newString.includes(',')) {
    newString = Number(newString).toLocaleString()
  }
  return newString
}