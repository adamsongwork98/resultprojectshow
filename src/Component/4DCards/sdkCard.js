import React, { Component } from 'react';
import { connect } from 'react-redux'
import sandagen from '../../Images/sandagen.png';
import { language } from '../../language';
import Moment from 'moment-timezone';
import { Link } from 'react-router-dom';
import { characterList } from '../../static'

class sdkCard extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const Special = [1,2,3,4,5,6,7,8,9,10,11,12,13]
    const consolation = [14,15,16,17,18,19,20,21,22,23]
    return (
      <div className="sec-title col-sm-12 col-md-4 px-3">
        <div className="sandagenborder" style={{ marginBottom: 10 }}>
          <div style={{ display: 'flex', marginBottom: 2, border: '5px' }}>
            <div style={{ width: 'calc(100% / 3)' }}>
              <img src={sandagen} alt="Logo" className="logoimages"/>
            </div>
            <div className="textalignment" style={{ width: 'calc(100% / 3)' }}><b>{language[this.props.currentLanguage].sandakan}</b></div>
            <div className="textalignment2" style={{ width: 'calc(100% / 3)' }}>
              <div style={{ fontSize: 12, fontWeight: 'bold', marginBottom: 5 }}>{this.props.sdk.drawNo || '---/--'}</div>
              <div style={{ fontSize: 12, fontWeight: 'bold' }}>{Moment(this.props.date).format('DD-MMM-YYYY (ddd)')}</div>
            </div>
          </div>
        </div>
        <div style={{ display: 'flex', marginBottom: 10 }}>
          <div className="dcc sandagenborderclass" style={{ width: 'calc(100% / 3)', marginLeft: 4, fontWeight: 'bold' }}>
            {language[this.props.currentLanguage].first}
          </div>
          <div className="dcc sandagenborderclass" style={{ width: 'calc(100% / 3)', marginLeft: 4, marginRight: 4, fontWeight: 'bold' }}>
            {language[this.props.currentLanguage].second}
          </div>
          <div className="dcc sandagenborderclass" style={{ width: 'calc(100% / 3)', fontWeight: 'bold' }}>
            {language[this.props.currentLanguage].third}
          </div>
        </div>
        <div className="numberborder" style={{ marginBottom: 10, fontWeight: 'bold' }}>
          <div style={{ display: 'flex', marginBottom: 2, border: '5px' }}>
            <Link style={{ width: 'calc(100% / 3)', borderradius: '5px', color: 'black', textDecoration: 'none', position: 'relative' }} to={{ pathname: "/FourDHistory", data: this.props.sdk.P1 || '----' }} >
              {this.props.sdk.P1OriPosition && <span style={{ color: 'red', position: 'absolute', top: -6, left: 3, fontSize: '15px' }}>{characterList[this.props.sdk.P1OriPosition]}</span>}
              {this.props.sdk.P1 || '----'}
            </Link>
            <Link style={{ width: 'calc(100% / 3)', borderradius: '5px', color: 'black', textDecoration: 'none', position: 'relative' }} to={{ pathname: "/FourDHistory", data: this.props.sdk.P2 || '----' }} >
              {this.props.sdk.P2OriPosition && <span style={{ color: 'red', position: 'absolute', top: -6, left: 3, fontSize: '15px' }}>{characterList[this.props.sdk.P2OriPosition]}</span>}
              {this.props.sdk.P2 || '----'}
            </Link>
            <Link style={{ width: 'calc(100% / 3)', borderradius: '5px', color: 'black', textDecoration: 'none', position: 'relative' }} to={{ pathname: "/FourDHistory", data: this.props.sdk.P3 || '----' }} >
              {this.props.sdk.P3OriPosition && <span style={{ color: 'red', position: 'absolute', top: -6, left: 3, fontSize: '15px' }}>{characterList[this.props.sdk.P3OriPosition]}</span>}
              {this.props.sdk.P3 || '----'}
            </Link>
          </div>
        </div>
        <div style={{ display: 'flex', marginBottom: 10 }}>
          <div className = "dcc sandagenborderclass" style={{ width: 'calc(100% / 1)', fontWeight: 'bold' }}>
            {language[this.props.currentLanguage].special}
          </div>
        </div>
        <div className="numberborderdspecon" style={{ marginBottom: 10, fontWeight: 'bold' }}>
          <div style={{
            display: 'flex',
            flexWrap: 'wrap',
          }}
          >
            {
              Special
                ? Special.map((item, index) => {
                  let itemList = [<Link key={index} style={{ width: 'calc(100% / 5)', position: 'relative', padding: '8px', color: 'black', textDecoration: 'none' }}
                    to={{ pathname: "/FourDHistory", data: this.props.sdk[item] || '----' }} >
                    <span style={{ color: 'red', position: 'absolute', top: 0, left: 3, fontSize: '15px' }}>{characterList[item]}</span>
                    {this.props.sdk[item] || '----' }
                  </Link>]
                  if (index === 10) {
                    itemList.unshift(<div key={'14'} style={{ width: 'calc(100% / 5)', padding: '8px' }}>
                      {' '}
                    </div>)
                  }
                  return itemList
                })
                : [...Array(13)].map((item, index) => <div key={index} style={{ width: 'calc(100% / 5)' }}>----</div>)
            }
          </div>
        </div>
        <div style={{ display: 'flex', marginBottom: 10 }}>
          <div className = "dcc sandagenborderclass" style={{ width: 'calc(100% / 1)', fontWeight: 'bold' }}>
            {language[this.props.currentLanguage].consolation}
          </div>
        </div>
        <div className="numberborderdspecon" style={{ marginBottom: 30, fontWeight: 'bold' }}>
          <div style={{
            display: 'flex',
            flexWrap: 'wrap',
          }}
          >
            {
              consolation
                ? consolation.map((item, index) => <Link key={index} style={{ width: 'calc(100% / 5)', position: 'relative', padding: '8px', color: 'black', textDecoration: 'none' }}
                  to={{ pathname: "/FourDHistory", data: this.props.sdk[item] || '----' }} >
                  <span style={{ color: 'red', position: 'absolute', top: 0, left: 3, fontSize: '15px' }}>{characterList[item]}</span>
                  {this.props.sdk[item] || '----' }
                </Link>)
                : [...Array(10)].map((item, index) => <div key={index} style={{ width: 'calc(100% / 5)' }}>----</div>)
            }
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentLanguage: state.currentLanguage,
  }
}

export default connect(mapStateToProps, null)(sdkCard)
