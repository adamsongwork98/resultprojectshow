import React, { Component } from 'react';
import { connect } from 'react-redux'
import magnumlogo from '../../Images/magnum.png';
import LiveLogo from '../../Images/Live.gif';
import { language } from '../../language';
import Moment from 'moment-timezone';
import { Link } from 'react-router-dom';
import { isMobile } from 'react-device-detect';
import { characterList } from '../../static'
import { filterJackpotNumber } from '../../utility/filterJackpotNumber';

class MagnumCard extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const Special = [1,2,3,4,5,6,7,8,9,10,11,12,13]
    const consolation = [14,15,16,17,18,19,20,21,22,23]
    return (
      <div className="sec-title col-sm-12 col-md-4 px-3">
        <div className="magnumborder" style={{ marginBottom: 10 }}>
          <div style={{ display: 'flex', marginBottom: 2, border: '5px' }}>
            <div style={{ width: 'calc(100% / 3)' }}>
              <img src={magnumlogo} alt="Logo" className={`logomagnumimages ${isMobile && 'mobileCardLogo'}`}/>
            </div>
            <div className="textalignment" style={{ width: 'calc(100% / 3)', position: 'relative' }}>
              <b>
                {language[this.props.currentLanguage].magnum} 4D
                {Moment().hours() >= 19 && <Link target="_blank" to={{ pathname: "https://www.magnum4d.my/en/"}} >
                  <img src={LiveLogo} style={{ position: 'absolute', top: -25, right: -30, width: 60, height: 30}} alt="Live" />
                </Link>}
              </b>
            </div>
            <div className="textalignment2" style={{ width: 'calc(100% / 3)' }}>
              <div style={{ fontSize: 12, fontWeight: 'bold', marginBottom: 5 }}>{this.props.mag.drawNo || '---/--'}</div>
              <div style={{ fontSize: 12, fontWeight: 'bold' }}>{Moment(this.props.date).format('DD-MMM-YYYY (ddd)')}</div>
            </div>
          </div>
        </div>
        <div style={{ display: 'flex', marginBottom: 10 }}>
          <div className="dcc bordermagnumclass" style={{ width: 'calc(100% / 3)', marginLeft: 4, fontWeight: 'bold' }}>
            {language[this.props.currentLanguage].first}
          </div>
          <div className="dcc bordermagnumclass" style={{ width: 'calc(100% / 3)', marginLeft: 4, marginRight: 4, fontWeight: 'bold' }}>
            {language[this.props.currentLanguage].second}
          </div>
          <div className="dcc bordermagnumclass" style={{ width: 'calc(100% / 3)', fontWeight: 'bold' }}>
            {language[this.props.currentLanguage].third}
          </div>
        </div>
        <div className="numberborder" style={{ marginBottom: 10, fontWeight: 'bold' }}>
          <div style={{ display: 'flex', marginBottom: 2 }}>
            <Link style={{ width: 'calc(100% / 3)', borderradius: '5px', color: 'black', position: 'relative', textDecoration: 'none' }} to={{ pathname: "/FourDHistory", data: this.props.mag.P1 || '----' }} >
              {this.props.mag.P1OriPosition && <span style={{ color: 'red', position: 'absolute', top: -6, left: 3, fontSize: '15px' }}>{characterList[this.props.mag.P1OriPosition]}</span>}
              {this.props.mag.P1 || '----'}
            </Link>
            <Link style={{ width: 'calc(100% / 3)', borderradius: '5px', color: 'black', position: 'relative', textDecoration: 'none' }} to={{ pathname: "/FourDHistory", data: this.props.mag.P2 || '----' }} >
              {this.props.mag.P2OriPosition && <span style={{ color: 'red', position: 'absolute', top: -6, left: 3, fontSize: '15px' }}>{characterList[this.props.mag.P2OriPosition]}</span>}
              {this.props.mag.P2 || '----'}
            </Link>
            <Link style={{ width: 'calc(100% / 3)', borderradius: '5px', color: 'black', position: 'relative', textDecoration: 'none' }} to={{ pathname: "/FourDHistory", data: this.props.mag.P3 || '----' }} >
              {this.props.mag.P3OriPosition && <span style={{ color: 'red', position: 'absolute', top: -6, left: 3, fontSize: '15px' }}>{characterList[this.props.mag.P3OriPosition]}</span>}
              {this.props.mag.P3 || '----'}
            </Link>
          </div>
        </div>
        <div style={{ display: 'flex', marginBottom: 10, fontSize: '0.9vw' }}>
          <div className = "dcc bordermagnumclass" style={{ width: 'calc(100% / 1)', fontWeight: 'bold' }}>
            {language[this.props.currentLanguage].special}
          </div>
        </div>
        <div className="numberborderdspecon" style={{ marginBottom: 10, fontWeight: 'bold' }}>
          <div style={{
            display: 'flex',
            flexWrap: 'wrap',
          }}
          >
            {
              Special
                ? Special.map((item, index) => {
                  let itemList = []
                  itemList.push(
                    <Link key={index} style={{ width: 'calc(100% / 5)', padding: '8px', color: 'black', position: 'relative', textDecoration: 'none' }}
                      to={{ pathname: "/FourDHistory", data: this.props.mag[item] || '----' }} >
                      <span style={{ color: 'red', position: 'absolute', top: 0, left: 3, fontSize: '15px' }}>{characterList[item]}</span>
                      {this.props.mag[item] || '----' }
                    </Link>
                  )
                  if (index === 10) {
                    itemList.unshift(<div key={'14'} style={{ width: 'calc(100% / 5)', padding: '8px' }}>
                      {' '}
                    </div>)
                  }
                  return itemList
                })
                : [...Array(13)].map((item, index) => <div key={index} style={{ width: 'calc(100% / 5)' }}>----</div>)
            }
          </div>
        </div>
        <div style={{ display: 'flex', marginBottom: 10 }}>
          <div className = "dcc bordermagnumclass" style={{ width: 'calc(100% / 1)', fontWeight: 'bold' }}>
            {language[this.props.currentLanguage].consolation}
          </div>
        </div>
        <div className="numberborderdspecon" style={{ marginBottom: 10, fontWeight: 'bold' }}>
          <div style={{
            display: 'flex',
            flexWrap: 'wrap',
          }}
          >
            {
              consolation
                ? consolation.map((item, index) =>
                  <Link key={index} style={{ width: 'calc(100% / 5)', padding: '8px', color: 'black', position: 'relative', textDecoration: 'none' }}
                    to={{ pathname: "/FourDHistory", data: this.props.mag[item] || '----' }} >
                    <span style={{ color: 'red', position: 'absolute', top: 0, left: 3, fontSize: '15px' }}>{characterList[item]}</span>
                    {this.props.mag[item] || '----' }
                  </Link>
                )
                : [...Array(10)].map((item, index) => <div key={index} style={{ width: 'calc(100% / 5)' }}>----</div>)
            }
          </div>
        </div>
        <div style={{ display: 'flex', marginBottom: 10 }}>
          <div className = "dcc bordermagnumclass" style={{ width: 'calc(100% / 1)', fontWeight: 'bold' }}>
            {language[this.props.currentLanguage].JEstimated}
          </div>
        </div>
        <div className="numberborderdspecon" style={{ marginBottom: 30 }}>
          <div style={{ display: 'flex', marginBottom: 2 }}>
            <div style={{ width: 'calc(100% / 2)', borderradius: '5px', fontWeight: 'bold' }}>{language[this.props.currentLanguage].Jackpot} 1</div>
            <div style={{ width: 'calc(100% / 1)', borderradius: '5px', fontWeight: 'bold' }}>RM {filterJackpotNumber(this.props.mag.jackpot1) || '----'}</div>
          </div>
          <div style={{ display: 'flex', marginBottom: 2, border: '5px' }}>
            <div style={{ width: 'calc(100% / 2)', borderradius: '5px', fontWeight: 'bold' }}>{language[this.props.currentLanguage].Jackpot} 2</div>
            <div style={{ width: 'calc(100% / 1)', borderradius: '5px', fontWeight: 'bold' }}>RM {filterJackpotNumber(this.props.mag.jackpot2) || '----'}</div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentLanguage: state.currentLanguage,
  }
}

export default connect(mapStateToProps, null)(MagnumCard)
