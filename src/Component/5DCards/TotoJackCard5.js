import React, { Component } from 'react';
import { connect } from 'react-redux'
import { language } from '../../language';
import { filterJackpotNumber } from '../../utility/filterJackpotNumber';

class TotoJackCard5 extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const totosup = [1,2,3,4,5,6]
    return (
      <div className="sec-title col-sm-12 col-md-4 px-3">
        <div style={{ display: 'flex', marginBottom: 10 }}>
          <div className = "dcc totoborderclass" style={{ width: 'calc(100% / 1)', fontWeight: 'bold', color: 'white' }}>
            {language[this.props.currentLanguage].SupremeToto} 6/58
          </div>
        </div>
        <div className="numberbordergorjackport2" style={{ marginBottom: 30 }}>
          <div className="dcc border-bottom border-dark-solid" style={{ display: 'flex', marginBottom: 2, fontWeight: 'bold', padding: '8px' }}>
            {
              totosup
                ? totosup.map((item, index) => <div className="number5Dborderdspecon" key={index} style={{ width: 'calc(100% / 6)' }}>{this.props.totosup[item] || '----' }</div>)
                : [...Array(10)].map((item, index) => <div key={index} style={{ width: 'calc(100% / 6)' }}>----</div>)
            }
          </div>
          <div style={{ display: 'flex', marginBottom: 2, border: '5px', fontWeight: 'bold', padding: '8px' }}>
            <div className="dcc number5Dborderdspecon" style={{ width: 'calc(100% / 2)', borderradius: '5px', height: '35px' }}>{language[this.props.currentLanguage].Jackpot} :</div>
            <div className="dcc border-dark-ridge number5Dborderdspecon" style={{ width: 'calc(100% / 1)', borderradius: '5px' }}>RM  {filterJackpotNumber(this.props.totosup.jackpot1) || '----'}</div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentLanguage: state.currentLanguage,
  }
}

export default connect(mapStateToProps, null)(TotoJackCard5)
